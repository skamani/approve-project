## Example project: Show Manual Approval

This project shows how a pipeline can be configured with a manual. set of jobs to approve or deny something.

Two jobs are created with some shell scripting in them to generate artifacts based on which job is selected by the user.

Only users who have access to the pipelines can click the play button.

For configuration example, see .gitlab-ci.yml file.

Addtional configuration in the cancel_pipeline.sh file takes a private token to actually cancel the pipeline.  For this, create 
a Project Access Token, and set it in the Project's Settings > CI/CD > Variables area as a Masked variable to avoid any leaks.

An additional configuration can also log a "Note" on a specific issue using a secondary API call that can be triggered from the
cancel_pipeline.sh file.
